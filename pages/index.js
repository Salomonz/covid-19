import React from 'react';
import Head from 'next/head';
import MaterialTable from 'material-table';
import Search from '@material-ui/icons/Search'

function CaseTable() {
  return (
    <div>
      <Head>
        <title>COVID-19</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" key="viewport" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
      </Head>

      <MaterialTable style={{ maxWidth: 800 }}
        icons={{
          Search: Search,
        }}
        title="COVID-19"
        columns={[
          {
            title: 'Country', field: 'country',
            cellStyle: {
              width: 150,
              maxWidth: 150,
              minWidth:100
            },
            headerStyle: {
              width: 150,
              maxWidth: 150,
              minWidth:100
            }
          },
          {
            title: 'Total Confirmed Cases', field: 'cases', type: 'numeric', defaultSort: 'desc',
            cellStyle: {
              width: 250,
              maxWidth: 250,
              minWidth:100
            },
            headerStyle: {
              width: 250,
              maxWidth: 250,
              minWidth:100
            }
          },
          {
            title: 'Recovered', field: 'recovered', type: 'numeric',
            cellStyle: {
              minWidth:80,
              color: 'rgb(47, 134, 69)'
            },
            headerStyle: {
              minWidth:80
            }
          },
          {
            title: 'Deaths', field: 'deaths', type: 'numeric',
            cellStyle: {
              minWidth:70,
              color: '#fd4e41'
            },
            headerStyle: {
              minWidth:70
            }
          },
        ]}
        data={query =>
          new Promise((resolve, reject) => {
            let proxyUrl = 'https://cors-anywhere.herokuapp.com/',
              url = 'https://interactive-static.scmp.com/sheet/wuhan/viruscases.json'
            fetch(proxyUrl + url)
              .then(response => response.json())
              .then(result => {
                resolve({
                  data: result.entries, //.country.filter(c => c.contains(query.search))
                  //page: result.page - 1,
                  totalCount: result.total,
                })
              })
          })
        }
        options={{
          headerStyle: {
            backgroundColor: '#01579b',
            color: '#FFF'
          },
          paging: false,
          search: false
        }}
        localization={{
          pagination: 'false',
          toolbar: {
            nRowsSelected: '{0} row(s) selected'
          },
          //header: {
            //actions: 'Actions'
          //},
          body: {
            emptyDataSourceMessage: 'No records to display',
            //filterRow: {
              //filterTooltip: 'Filter'
            //}
          }
        }}
      />
    </div>
  )
}

export default CaseTable;
